package eu.lundegaard.javaarearoulette.controller;

import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.HandlerMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@RestController
public class RestApiController {

    @Value("${confluence.username:username_not_set}")
    private String username;

    @Value("${confluence.password:password_not_set}")
    private String password;

    @Value("${confluence.url:http://wiki.lnd.bz}")
    private String url;

    private final OkHttpClient client;

    private String currentSessionHeader;

    public RestApiController(OkHttpClient client) {
        this.client = client;
    }

    @GetMapping("/avatars")
    public String avatars() throws IOException {
        final RequestBody body = new FormBody.Builder()
                .add("os_username", username)
                .add("os_password", password)
                .add("login", "Log in")
                .build();
        Request authRequest = new Request.Builder()
                .url(url + "/dologin.action")
                .addHeader("Content-Type", "application/x-www-form-urlencoded")
                .addHeader("Cache-Control", "cache")
                .post(body)
                .build();

        Response response = client.newCall(authRequest).execute();

        if (!response.isSuccessful()) throw new IOException("Unexpected code " + response);

        currentSessionHeader = response.header("Set-Cookie");

        Request getAvatars = new Request.Builder()
                .url(url + "/rest/api/group/area java/member")
                .addHeader("Cookie", currentSessionHeader)
                .get()
                .build();

        final Response avatarsResponse = client.newCall(getAvatars).execute();

        if (!avatarsResponse.isSuccessful()) throw new IOException("Unexpected code " + avatarsResponse);

        if (avatarsResponse.body() == null) {
            throw new IOException("No avatars received from conflu.");
        }
        return avatarsResponse.body().string();
    }

    @GetMapping("/pictures/**")
    public void pictures(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String restOfTheUrl = (String) request.getAttribute(
                HandlerMapping.PATH_WITHIN_HANDLER_MAPPING_ATTRIBUTE);
        final String imgPath = restOfTheUrl.replace("/pictures/", "");

        final Request picture = new Request.Builder()
                .url(url + "/" + imgPath)
                .addHeader("Cookie", currentSessionHeader)
                .get()
                .build();

        final Response result = client.newCall(picture).execute();
        if (result.isSuccessful())

            response.setHeader("Content-Type", result.header("Content-Type"));
        response.getOutputStream().write(result.body().bytes());
    }
}
