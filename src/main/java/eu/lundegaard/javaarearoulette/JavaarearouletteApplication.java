package eu.lundegaard.javaarearoulette;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JavaarearouletteApplication {

	public static void main(String[] args) {
		SpringApplication.run(JavaarearouletteApplication.class, args);
	}

}
