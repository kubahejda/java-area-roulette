FROM adoptopenjdk/openjdk13-openj9

ARG file_name

RUN mkdir /opt/app
COPY target/$file_name.jar /opt/app/app.jar
CMD ["java", "-XX:InitialRAMPercentage=50", "-XX:MaxRAMPercentage=80", "-XX:+IdleTuningGcOnIdle", "-Xtune:virtualized", "-jar", "-Dspring.profiles.active=cloud", "/opt/app/app.jar"]
