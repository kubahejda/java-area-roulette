# Java area roulette

This particular application serves as a tool to randomly select one member of Java area. 
Can be used for any purpose, but the typical usage is the final part of
Java Area meeting, when this tool selects the next Java Area member to host the meeting.

Members are taken from the confluence, where all of them are grouped together and can dynamically
change during the year.

The app is based on Java Spring backend and a simple JS frontend. The main page calls
the BE for pictures of members. The BE authorizes to conflu and with JSESSION cookie calls 
the confluence REST API to retrieve paths to profile images and names of members. This info
is provided to FE and it calls be again for retrieving pictures from given paths. In this 
second call the BE serves more or less like a proxy, but is necessary, because it holds the 
JSESSION token and is authorized as we do not want to provide connection credentials on FE.

##How to run the app
You just need to set up 4 parameters. This can be done directly in application.yml, provided
to app as environment variable or provided to dockerized app as an environment variable.
 
- CONFLUENCE_USERNAME
- CONFLUENCE_PASSWORD
- CONFLUENCE_URL

Example `docker-compose` is attached, but remember to install and then run docker:build maven
goal and set up env variables in docker-compose accordingly.